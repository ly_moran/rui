package ${package};

import io.mybatis.common.core.DataResponse;
import io.mybatis.common.core.RowsResponse;

import ${project.attrs.basePackage}.model.${it.name.className};
import ${project.attrs.basePackage}.service.${it.name.className}Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ${it.name} - ${it.comment}
 *
 * @author 系统自动生成
 */
@RestController
@RequestMapping("${it.name.fieldName.s}")
@AllArgsConstructor
public class ${it.name.className}Controller {

  private final ${it.name.className}Service ${it.name.fieldName}Service;

  @PostMapping
  public RestResult save(@RequestBody ${it.name.className} ${it.name.fieldName}) {
    ${it.name.fieldName}Service.save( ${it.name.fieldName})
    return RestResult.success();
  }

  @GetMapping
  public RestResult findList(@RequestBody ${it.name.className} ${it.name.fieldName}) {
    return RestResult.success(${it.name.fieldName}Service.findList( ${it.name.fieldName}));
  }

  @GetMapping(value = "/{id}")
  public RestResult findById(@PathVariable("id") Long id) {
    return RestResult.success(${it.name.fieldName}Service.findById(id));
  }

  @PutMapping(value = "/{id}")
  public RestResult<${it.name.className}> update(@PathVariable("id") Long id, @RequestBody ${it.name.className} ${it.name.fieldName}) {
    ${it.name.fieldName}.setId(id);
    return RestResult.success(${it.name.fieldName}Service.update( ${it.name.fieldName}));
  }

  @DeleteMapping(value = "/{id}")
  public RestResult deleteById(@PathVariable("id") Long id) {
    return RestResult.success(${it.name.fieldName}Service.deleteById(id) == 1);
  }

}

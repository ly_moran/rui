package ${package};

import io.mybatis.service.BaseService;

import ${project.attrs.basePackage}.model.${it.name.className};

/**
 * ${it.name} - ${it.comment}
 *
 * @author 系统自动生成
 */
public interface ${it.name.className}Service extends BaseService<${it.name.className}, Long> {

}

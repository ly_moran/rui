package ${package};

import com.moran.controller.Controller;
import ${project.attrs.basePackage}.service.${it.name.className}Service;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author : moran
 * @date : 2024/5/9 14:30
 */
@RestController
@RequestMapping("/${it.name.fieldName.s}")
@AllArgsConstructor
public class ${it.name.className}Controller extends Controller{

  private final ${it.name.className}Service ${it.name.fieldName}Service;

}
